<?php namespace Entopancore\Project\Services;

use Illuminate\Support\ServiceProvider;
use Entopancore\Project\Classes\ProjectAuthManager;

class ProjectServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if (\File::exists(__DIR__ . '/../vendor/autoload.php')) {
            include __DIR__ . '/../vendor/autoload.php';
        }


        $configPath = __DIR__ . '/../config/project.php';
        $this->publishes([$configPath => config_path('project.php')], 'config');


        \App::singleton('project.auth', function () {
            return ProjectAuthManager::instance();
        });

        \App::register(\Entopancore\Project\Services\RouteServiceProvider::class);
        \App::register(\Entopancore\Project\Services\SystemServiceProvider::class);
        \App::register(\Entopancore\Project\Services\EventServiceProvider::class);
        \App::register(\Entopancore\Project\Services\BackendServiceProvider::class);


    }
}