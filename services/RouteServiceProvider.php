<?php namespace Entopancore\Project\Services;

use File;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public $config;

    public function __construct(\Illuminate\Contracts\Foundation\Application $app)
    {
        parent::__construct($app);

    }

    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $companies = scandir(plugins_path());
        foreach ($companies as $company) {
            if ($company === '.' or $company === '..' or $company === '.DS_Store') continue;
            $plugins = scandir(plugins_path() . "/" . $company);
            foreach ($plugins as $plugin) {
                if ($plugin === '.' or $plugin === '..' or $plugin === '.DS_Store') continue;
                $search = "$company/$plugin";
                if (!in_array($search, config("entopancore.project::excluded"))) {
                    $file = plugins_path("$search/routes.php");
                    if (File::exists($file)) {
                        require $file;
                    }
                }

            }
        }

        include __DIR__ . '/../routes.php';
    }

}