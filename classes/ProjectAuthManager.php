<?php namespace Entopancore\Project\Classes;

use Backend\Classes\AuthManager;

class ProjectAuthManager extends AuthManager
{
    protected static $instance;

    public function getUserRoleCode()
    {
        if ($this->getUser()->role) {
            return $this->user->role->code;
        }
        return false;
    }

    public function getUserId()
    {
        if ($user = $this->getUser()) {
            return $user->id;
        } else {
            return false;
        }
    }
}
