<?php namespace Entopancore\Project\Controllers;

use App;
use Lang;
use View;
use Flash;
use Config;
use Request;
use Backend;
use Session;
use Redirect;
use Response;
use BackendAuth;
use Backend\Models\Preference as BackendPreference;
use October\Rain\Exception\SystemException;

/**
 * The Backend base controller class, used by Backend controllers.
 * The base controller services back end pages.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Controller extends Backend\Classes\Controller
{

    public function run($action = null, $params = [])
    {
        $this->action = $action;
        $this->params = $params;

        /*
         * Check security token.
         */
        if (!$this->verifyCsrfToken()) {
            return Response::make(Lang::get('backend::lang.page.invalid_token.label'), 403);
        }

        /*
         * Check forced HTTPS protocol.
         */
        if (!$this->verifyForceSecure()) {
            return Redirect::secure(Request::path());
        }

        /*
         * Determine if this request is a public action.
         */
        $isPublicAction = in_array($action, $this->publicActions);

        /*
         * Check that user is logged in and has permission to view this page
         */
        if (!$isPublicAction) {

            /*
             * Not logged in, redirect to login screen or show ajax error.
             */
            if (!BackendAuth::check()) {
                return Request::ajax()
                    ? Response::make(Lang::get('backend::lang.page.access_denied.label'), 403)
                    : Backend::redirectGuest('backend/auth');
            }

            /*
             * Check access groups against the page definition
             */
            if ($this->requiredPermissions && !$this->user->hasAnyAccess($this->requiredPermissions)) {
                return Response::make(View::make('backend::access_denied'), 403);
            }
        }

        /*
         * Extensibility
         */
        if ($event = $this->fireSystemEvent('backend.page.beforeDisplay', [$action, $params])) {
            return $event;
        }

        /*
         * Set the admin preference locale
         */
        BackendPreference::setAppLocale();
        BackendPreference::setAppFallbackLocale();

        /*
         * Execute AJAX event
         */
        if ($ajaxResponse = $this->execAjaxHandlers()) {
            return $ajaxResponse;
        }

        /*
         * Execute postback handler
         */
        if (
            ($handler = post('_handler')) &&
            ($handlerResponse = $this->runAjaxHandler($handler)) &&
            $handlerResponse !== true
        ) {
            return $handlerResponse;
        }

        /*
         * Execute page action
         */
        try {
            $result = $this->execPageAction($action, $params);
        } catch (SystemException $e) {
            return \View::make('backend::access_denied');
        }

        if (!is_string($result)) {
            return $result;
        }

        return Response::make($result, $this->statusCode);
    }

}
