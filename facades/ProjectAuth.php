<?php namespace Entopancore\Project\Facades;

use October\Rain\Support\Facade;

class ProjectAuth extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'project.auth';
    }
}
