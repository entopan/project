<?php namespace Entopancore\Project;

use System\Classes\PluginBase;
use Validator;

/**
 * Utility Plugin Information File
 */
class Plugin extends PluginBase
{


    public function register()
    {

    }

    public function pluginDetails()
    {
        return [
            'name' => 'Project',
            'description' => 'Project',
            'author' => 'Entopancore',
            'icon' => 'icon-leaf'
        ];
    }


}
