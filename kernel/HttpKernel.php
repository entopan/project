<?php namespace Entopancore\Project\Kernel;

use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode;
use Spatie\Cors\Cors;

class HttpKernel extends \October\Rain\Foundation\Http\Kernel
{
    /**
     * The bootstrap classes for the application.
     *
     * @var array
     */
    protected $bootstrappers = [
        '\October\Rain\Foundation\Bootstrap\RegisterClassLoader',
        '\October\Rain\Foundation\Bootstrap\LoadEnvironmentVariables',
        '\October\Rain\Foundation\Bootstrap\LoadConfiguration',
        '\October\Rain\Foundation\Bootstrap\LoadTranslation',
        \Illuminate\Foundation\Bootstrap\HandleExceptions::class,
        \Illuminate\Foundation\Bootstrap\RegisterFacades::class,
        '\October\Rain\Foundation\Bootstrap\RegisterOctober',
        \Illuminate\Foundation\Bootstrap\RegisterProviders::class,
        \Illuminate\Foundation\Bootstrap\BootProviders::class,
    ];

    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        CheckForMaintenanceMode::class,
        Cors::class
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \October\Rain\Cookie\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
        'api' => [
            'bindings',
            \Entopancore\Api\Http\Middleware\CheckLang::class,
            \Entopancore\Api\Http\Middleware\Api::class,
        ],
        'protected' => [
            'bindings',
            \Entopancore\Api\Http\Middleware\CheckLang::class,
            \Entopancore\Api\Http\Middleware\AddBearerTokenFromGetToHeader::class,
            \Tymon\JWTAuth\Middleware\GetUserFromToken::class,
            \Entopancore\Api\Http\Middleware\Api::class
        ],
        'authenticated' => [
            \Entopancore\Api\Http\Middleware\BearerIsAuthenticated::class,
            \Entopancore\Api\Http\Middleware\CheckLang::class,
            \Entopancore\Api\Http\Middleware\Api::class
        ],
        'check' => [
            \Tymon\JWTAuth\Middleware\GetUserFromToken::class
        ],
        'file' => [
            \Entopancore\Api\Http\Middleware\CheckLang::class,
            \Entopancore\Api\Http\Middleware\CheckFileOwner::class,
            \Entopancore\Api\Http\Middleware\Api::class
        ],
    ];

    /**
     * The priority-sorted list of middleware.
     *
     * Forces the listed middleware to always be in the given order.
     *
     * @var array
     */
    protected $middlewarePriority = [
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class
    ];
}