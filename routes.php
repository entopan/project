<?php

App::error(function (\Illuminate\Foundation\Http\Exceptions\MaintenanceModeException $exception) {
    return 'Upgrading...';
});

App::error(function (\Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException $exception) {
    return 'Route not exist or method is not allowed';
});


Route::group([
    'middleware' => ['web'],
    'prefix' => Config::get('cms.backendUri', 'backend')
], function () {
    Route::any('{slug}', 'Entopancore\Project\Controllers\BackendController@run')->where('slug', '(.*)?');
});

Route::any(Config::get('cms.backendUri', 'backend'), 'Entopancore\Project\Controllers\BackendController@run')->middleware('web');
